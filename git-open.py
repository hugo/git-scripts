#!/usr/bin/env python3

"""Figure out the HTTP source of a remote, and open it in a browser."""

import sys
import subprocess
import argparse
import re
import os.path


def err(s):
    """Print error message."""
    print("\x1b[0;31mError\x1b[m " + s, file=sys.stderr)


def warn(s):
    """Print warning message."""
    print("\x1b[0;33mWarn\x1b[m " + s, file=sys.stderr)


def info(s):
    """Print info message."""
    print("\x1b[0;32mInfo\x1b[m " + s, file=sys.stderr)


try:
    # package python-pyxdg on Arch
    from xdg.BaseDirectory import xdg_config_dirs
except ModuleNotFoundError:
    warn("xdg module not found, defaulting to $HOME/.config")
    import os
    home = os.getenv("HOME")
    xdg_config_dirs = [os.path.join(home, ".config")]

can_load_configuration = False
try:
    # package python-yaml on Arch
    import yaml
    can_load_configuration = True
except ModuleNotFoundError:
    warn("yaml module not found, configuration will NOT be loaded")


configuration = {
    # a list of dictionaries, each containing the keys 'i' and 'o'
    'patterns': []
}


def popen(str):
    """Exec str, and return its stdout."""
    p = subprocess.Popen(str.split(" "),
                         universal_newlines=True,
                         stdout=subprocess.PIPE)
    out, err = p.communicate()
    p.wait()
    if p.returncode != 0:
        sys.exit(1)
    return out


def gitconf(field):
    """Get git config field, or die."""
    return popen("git config " + field)


def remote_url(remote_name):
    """Get url for git remote by name."""
    # return gitconf("remote.{}.url".format(remote_name))
    return popen(f'git remote get-url {remote_name}').strip()


def to_http(url):
    """
    Convert url into matching HTTP url.

    Uses configuration['patterns'] to check for special cases,
    otherwise anything starting with http is returned verbatim, and
    git@ is replaced with https://.
    """
    for pattern in configuration['patterns']:
        if match := re.match(pattern['i'], url):
            rx = re.compile('[$]([0-9]+)')
            on = list(pattern['o'])
            for m in reversed(list(rx.finditer(pattern['o']))):
                try:
                    on[m.start():m.end()] = match[int(m[1])]
                except ValueError as e:
                    print('All output patterns must be numbers')
                    # TODO fail catastrophically instead of
                    # re-raising
                    raise e
            return ''.join(on)

    if url[0:4] == "http":
        return url
    if url[0:4] == "git@":
        base, path = url[4:].split(":")
        return "https://" + base + "/" + path
    raise Exception("URL doesn't start with either 'http' or 'git@'")


def xdg_open(item):
    """Run xdg-open on argument."""
    subprocess.run(["xdg-open", item])


def open_remote(remote):
    """Open url for the named remote."""
    url = to_http(remote_url(remote))
    xdg_open(url)
    info(f'opening {url}')


def load_configuration():
    """Load configuration file, updates `configuration`."""
    global configuration
    # TODO possibly add ~/.config/git/open.yaml to list
    for dir in xdg_config_dirs:
        try:
            with open(os.path.join(dir, 'git-open.yaml')) as f:
                conf = yaml.unsafe_load(f)
                configuration |= conf
                break
        except FileNotFoundError:
            pass


def main(args):
    """Entry point, args should come from ArgumentParser.parse_args."""
    out = popen("git remote")
    remotes = out.strip().split("\n")

    remote = args.remote

    do = open_remote
    if args.dry_run:
        do = lambda x: print(to_http(remote_url(x)))  # noqa: E731

    if not remotes:
        err("No remotes")

    if remote:
        try:
            do(remote)
            return
        except Exception as e:
            warn(str(e))
            warn("Giving up")
            return

    for remote in remotes:
        try:
            do(remote)
            break
        except Exception as e:
            warn(str(e))
    else:
        err("All remotes failed")


if __name__ == "__main__":
    # Note that argparse gives `--help' and `-h', but git "eats"
    # `--help'. `-h' does however work.
    parser = argparse.ArgumentParser(
            description='open git remotes in web browser')
    parser.add_argument('-n', '--dry-run', action='store_true', dest='dry_run')
    parser.add_argument('remote', action='store', nargs='?')
    args = parser.parse_args()

    if can_load_configuration:
        load_configuration()

    main(args)
