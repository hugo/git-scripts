.PHONY: install

PREFIX := /usr
DESTDIR =

install:
	install git-children.py $(DESTDIR)$(PREFIX)/bin/git-children
	install git-ls.scm $(DESTDIR)$(PREFIX)/bin/git-ls
	install git-open.py $(DESTDIR)$(PREFIX)/bin/git-open
